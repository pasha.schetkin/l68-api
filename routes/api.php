<?php

use App\Http\Controllers\Api\V1\ArticlesController;
use App\Http\Controllers\Api\V1\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/



Route::prefix('v1')->group(function () {
  Route::apiResource('articles', ArticlesController::class)
    ->middleware('auth:sanctum');

  Route::post('auth/register', [AuthController::class, 'register'])->name('register');
  Route::post('auth/login', [AuthController::class, 'login'])->name('login');
  Route::post('auth/logout', [AuthController::class, 'logout'])->name('logout')
      ->middleware('auth:sanctum');

  Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
  });
});

