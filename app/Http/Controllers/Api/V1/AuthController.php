<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

/**
 * Class AuthController
 *
 * @group Auth
 * @package App\Http\Controllers\Api\V1
 *
 * API's for auth user
 */
class AuthController extends ApiV1Controller
{
    /**
     * @param RegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterRequest $request): \Illuminate\Http\JsonResponse
    {
        /** @var User $user */
        $user = User::create([
            'name' => $request->get('name'),
            'email' => strtolower($request->get('email')),
            'password' => Hash::make($request->get('password'))
        ]);
        return response()->json([
            'token' => $user->createToken($user->email)->plainTextToken,
            'user' => new UserResource($user)
        ], 201);
    }

    /**
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationException
     */
    public function login(LoginRequest $request): \Illuminate\Http\JsonResponse
    {
        $user = User::where('email', strtolower($request->get('email')))->first();

        if (!$user || !Hash::check($request->get('password'), $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        }

        return response()->json([
            'token' => $user->createToken($user->email)->plainTextToken,
            'user' => new UserResource($user)
        ], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request): \Illuminate\Http\JsonResponse
    {
        /** @var User $user */
        $user = $request->user();
        $user->tokens()->delete();
        return response()->json('', 204);
    }
}
