<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * Class ApiV1Controller
 * @package App\Http\Controllers\Api\V1
 *
 * @OA\Info(
 *     version="1.0",
 *     title="Example for response examples value"
 * )
 *
 * @OA\PathItem(path="/api/v1")
 */
abstract class ApiV1Controller extends Controller
{
    protected function success(array|Collection|Model $data = [])
    {
        return response()->json([
            'data' => $data,
            'message' => 'success'
        ]);
    }
}
