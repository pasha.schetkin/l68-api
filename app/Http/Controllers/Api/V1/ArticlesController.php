<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\Articles\ArticleStoreRequest;
use App\Http\Requests\Articles\UpdateRequest;
use App\Http\Resources\ArticleResource;
use App\Models\Article;

/**
 * @group Article management
 *
 * APIs for managing articles
 */
class ArticlesController extends ApiV1Controller
{
    /**
     * Display a listing of the resource.
     *
     * @apiResourceCollection App\Http\Resources\ArticleResource
     * @apiResourceModel App\Models\Article paginate=8
     * @transformerModel App\Models\User resourceKey=user
     */
    public function index()
    {
        $articles = Article::paginate(9);
        return ArticleResource::collection($articles);
    }

    /**
     * @OA\Post(
     *     path="/api/v1/articles",
     *     summary="Create new article",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="id",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="phone",
     *                     oneOf={
     *                     	   @OA\Schema(type="string"),
     *                     	   @OA\Schema(type="integer"),
     *                     }
     *                 ),
     *                 example={"id": "a3fb6", "name": "Jessica Smith", "phone": 12345678}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *             @OA\Examples(example="result", value={"success": true}, summary="An result object."),
     *             @OA\Examples(example="bool", value=false, summary="A boolean value."),
     *         )
     *     ),
     *      @OA\Response(
     *         response=422,
     *         description="Unprocessible entity",
     *         @OA\JsonContent(
     *             @OA\Examples(example="result", value={"message": "Error", "errors": {"FIELD": "error1"}}, summary="An result object.")
     *         )
     *     )
     * )
     */
    public function store(ArticleStoreRequest $request)
    {
        $article = Article::create($request->validated());
        return new ArticleResource($article);
    }

    /**
     * Get article by id.
     *
     */
    public function show(string $id)
    {
        $article = Article::find($id);
        if ($article) {
            return new ArticleResource($article);
        }
        return response()->json([
            'data' => null,
            'message' => 'not found'
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, string $id)
    {
      $article = Article::findOrFail($id);
      $this->authorize('update', $article);
      $article->update($request->validated());
      return new ArticleResource($article);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $article = Article::findOrFail($id);
        $this->authorize('delete', $article);
        $article->delete();
        return response()->json('', 204);
    }
}
