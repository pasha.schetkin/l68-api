<?php

namespace App\Http\Requests\Articles;

use Illuminate\Foundation\Http\FormRequest;

class ArticleStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'title' => ['bail', 'required', 'min:3', 'max:255'],
            'body' => ['bail', 'required', 'min:6', 'max:2000'],
            'user_id' => ['bail', 'required', 'exists:users,id']
        ];
    }

    public function bodyParameters()
    {
        return [
            'title' => [
                'description' => 'Title of the new article, must be min 3 characters max 255',
                'example' => 'Title new'
            ],
            'body' => [
                'description' => 'Body of the new article, must be min 6 characters max 2000',
                'example' => 'body for article'
            ],
            'user_id' => [
                'description' => 'User id'
            ]
        ];
    }
}
