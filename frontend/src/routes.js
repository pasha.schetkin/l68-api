import React from 'react';
import {createBrowserRouter} from 'react-router-dom';
import Home from "./components/Home";
import List from "./components/Articles/List";
import SignIn from "./components/Auth/Login";
import SignUp from "./components/Auth/Register";
import CreateArticle from "./components/Articles/CreateArticle";
import EditArticle from "./components/Articles/EditArticle";
import ShowArticle from "./components/Articles/Show";
const routers = createBrowserRouter([
  {
    path: "/",
    element: <Home/>,
  },
  {
    path: "/articles",
    element: <List/>
  },
  {
    path: "/login",
    element: <SignIn/>
  },
  {
    path: "/register",
    element: <SignUp/>
  },
  {
    path: "/article/create",
    element: <CreateArticle/>
  },
  {
    path: "/article/edit/:id",
    element: <EditArticle/>
  },
  {
    path: "/article/:id",
    element: <ShowArticle/>
  }
]);
export default routers;
