import logo from './logo.svg';
import './App.css';
import List from "./components/Articles/List";

function App() {
  return (
    <div className="App">
      <List/>
    </div>
  );
}

export default App;
