export default class PermanentStorage
{
  static getUser() {
    const user = localStorage.getItem('user') || false;
    if (user) {
      return JSON.parse(user);
    }
    return user;
  }

  static setItem(key, value) {
    let data = value;
    if (typeof value === 'object') {
      data = JSON.stringify(value);
    }
    localStorage.setItem(key, data);
  }

  static getItem(key) {
    return localStorage.getItem(key);
  }

  static removeItem(key) {
    localStorage.removeItem(key);
  }
}
