'use strict';

import Api from "./params/env";

function playWithResponsePromise(response) {
  return new Promise(function (resolve, reject) {
    if (response.status < 400) {
      response.json().then(resolve);
    } else {
      response.json().then(resolve);
    }
  });
}

export function get(url, token) {
  return fetch(url, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Credentials': true,
      credentials: 'same-origin',
      ...token && {'Authorization': 'Bearer ' + token}
    }
  }).then(playWithResponsePromise);
}

export function post(url, data, token) {
  return fetch(url, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Credentials': true,
      credentials: 'same-origin',
      ...token && {'Authorization': 'Bearer ' + token}
    },
    body: JSON.stringify(data)
  }).then(playWithResponsePromise);
}

export function put(url, data, token) {
  return fetch(url, {
    method: "PUT",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      ...token && {'Authorization': 'Bearer ' + token}
    },
    body: JSON.stringify(data)
  }).then(playWithResponsePromise);
}


export function remove(url, token) {
  return fetch(url, {
    method: "DELETE",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      ...token && {'Authorization': 'Bearer ' + token}
    }
  }).then();
}

export function getCsrf() {
  return fetch(Api.getHost() + 'sanctum/csrf-cookie', {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      credentials: 'include',
    }
  })
}
