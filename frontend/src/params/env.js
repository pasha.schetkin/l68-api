export const API_URL = 'http://127.0.0.1:8000/api/';

export const API_V1 = 'v1';

export default class Api {
  static urlV1(path) {
    return API_URL + API_V1 + path;
  }

  static getHost() {
    return 'http://127.0.0.1:8000/';
  }
}
