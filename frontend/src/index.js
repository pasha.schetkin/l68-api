import React from 'react';
import ReactDOM from 'react-dom';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import './index.css';

import CssBaseline from '@mui/material/CssBaseline';
import Container from '@mui/material/Container';

import {RouterProvider} from "react-router-dom";

import reportWebVitals from './reportWebVitals';
import routes from './routes';
import Navbar from "./components/common/Navbar";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Navbar/>
    <br/>
    <CssBaseline/>
    <Container>
      <RouterProvider router={routes}/>
    </Container>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
