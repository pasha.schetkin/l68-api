import {useParams} from "react-router-dom";
import PermanentStorage from "../../storage";
import {useEffect, useState} from "react";
import {get, getCsrf} from "../../request";
import Api from "../../params/env";
import Container from "@mui/material/Container";
import {LinearProgress} from "@mui/material";
import {createTheme} from '@mui/material/styles';
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";

export default function ShowArticle() {
  const params = useParams();
  const token = PermanentStorage.getItem('token') || false;
  useEffect(() => {
    if (params?.id && token) {
      getCsrf().then(r => {
        get(Api.urlV1(`/articles/${params.id}/`), token).then(response => {
          setArticle(response.data);
          setIsLoading(false);
        })
      })
    }
  }, []);
  const [isLoading, setIsLoading] = useState(false);
  const [article, setArticle] = useState(null);

  return (
    <Container component="main" maxWidth="lg">
      {isLoading && <LinearProgress />}
      {!isLoading && article &&
      <>
        <Card sx={{ minWidth: 450 }}>
          <CardContent>
            <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
              title:
            </Typography>
            <Typography variant="h5" component="div">
              {article.title}
            </Typography>
          </CardContent>
        </Card>
        <br/>
        <Card sx={{ minWidth: 450 }}>
          <CardContent>
            <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
              body:
            </Typography>
            <Typography variant="p">
              {article.body}
            </Typography>
          </CardContent>
        </Card>
        <br/>
        <Card sx={{ minWidth: 450 }}>
          <CardContent>
            <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
              author:
            </Typography>
            <Typography variant="div">
              {article.user.name}
            </Typography>
          </CardContent>
        </Card>
        <br/>
        <Button href="/articles">Back</Button>
      </>
      }
    </Container>
  );
}
