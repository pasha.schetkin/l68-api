import PermanentStorage from "../../storage";
import {useEffect, useState} from "react";
import {getCsrf, post, put} from "../../request";
import Api from "../../params/env";
import {Alert, LinearProgress, TextField} from "@mui/material";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";


export default function Form({article = {id: null, title: '', body: ''}, isCreate = true}) {
  const token = PermanentStorage.getItem('token') || false;

  useEffect(() => {
    if (!token) {
      window.location.href = '/login';
    }
  }, []);

  const [articleData, setArticleData] = useState(article);
  const [isLoading, setIsLoading] = useState(false);
  const [globalError, setGlobalError] = useState(null);
  const [validationError, setValidationErrors] = useState(null);

  const handleSubmit = (e) => {
    e.preventDefault();
    const user = PermanentStorage.getUser();
    if (!user) {
      window.location.href = '/login';
    }

    setIsLoading(true);
    const data = {
      ...articleData,
      user_id: user.id
    }

    getCsrf().then(r => {
      if (!isCreate) {
        put(Api.urlV1(`/articles/${article.id}`), data, token).then(response => {
          playResponse(response);
        })
      } else {
        post(Api.urlV1('/articles'), data, token).then(response => {
          playResponse(response);
        })
      }
    })
  }

  const playResponse = response => {
    setIsLoading(false);
    if (Object.keys(response).includes('data')) {
      window.location.href = '/articles'
    } else if (Object.keys(response).includes('error')) {
      setGlobalError(response.error);
    } else {
      setValidationErrors({message: response.message, errors: response.errors});
    }
  }

  const renderGlobalError = () => {
    let message = '';
    if (validationError) {
      message = validationError.message;
    } else if (globalError) {
      message = globalError;
    }

    if (message !== '') {
      return (
        <Alert severity="error">{message}</Alert>
      );
    }
  }

  return (
    <>
      {renderGlobalError()}
      {isLoading && <LinearProgress />}
      {!isLoading &&
        <Container component="main" maxWidth="lg">
          <Box
            sx={{
              marginTop: 8,
              display: 'flex',
              alignItems: 'center',
            }}
          >
            <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
              <TextField
                error={validationError?.errors && 'title' in validationError?.errors}
                margin="normal"
                required
                fullWidth
                id="title"
                label="Title"
                name="title"
                autoComplete="title"
                autoFocus
                value={articleData.title}
                onChange={(e) => setArticleData({...articleData, title: e.currentTarget.value})}
                helperText={validationError?.errors && validationError?.errors.title[0]}
              />
              <TextField
                error={validationError?.errors && 'body' in validationError?.errors}
                margin="normal"
                required
                fullWidth
                id="body"
                label="Content"
                name="body"
                value={articleData.body}
                rows={8}
                multiline
                onChange={(e) => setArticleData({...articleData, body: e.currentTarget.value})}
                helperText={validationError?.errors && validationError?.errors.body[0]}
              />

              <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
              >
                {isCreate ? 'Create article' : 'Update article'}
              </Button>
            </Box>
          </Box>
        </Container>
      }
    </>
  );
}
