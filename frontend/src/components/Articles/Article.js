import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import PermanentStorage from "../../storage";
import {useNavigate} from "react-router-dom";
import {getCsrf, remove} from "../../request";
import Api from "../../params/env";
import {CardHeader} from "@mui/material";
import { red } from '@mui/material/colors';
import EditNoteOutlinedIcon from '@mui/icons-material/EditNoteOutlined';
import PlaylistRemoveOutlinedIcon from '@mui/icons-material/PlaylistRemoveOutlined';


function Article(props) {
  const navigate = useNavigate();
  const token = PermanentStorage.getItem('token') || false;

  const goEdit = () => {
    navigate(`/article/edit/${props.article.id}`)
  }

  const handleDelete = () => {
    getCsrf().then(r => {
      remove(Api.urlV1(`/articles/${props.article.id}`), token).then(response => {
        window.location.href = '/articles';
      })
    })
  }

  return (
    <Card sx={{ minWidth: 275 }}>
      <CardHeader
        action={
          <>
            {props.article.can_edit && <EditNoteOutlinedIcon onClick={goEdit} cursor="pointer" titleAccess="Edit article" color="success" fontSize="large"/>}
            {props.article.can_edit && <PlaylistRemoveOutlinedIcon onClick={handleDelete} cursor="pointer" titleAccess="Delete article" sx={{ color: red[500] }} fontSize="large"/>}
          </>
        }
        title={
          <>
            <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
              # {props.article.id}
            </Typography>
          </>
        }
        subheader={props.article.created_at}
      />
      <CardContent>
        <Typography variant="h5" component="div" noWrap>
          {props.article.title}
        </Typography>
        <Typography sx={{ mb: 1.5 }} color="text.secondary" noWrap>
          {props.article.body}
        </Typography>
        <Typography variant="body2">
          Author: {props.article.user.name}
        </Typography>
      </CardContent>
      <CardActions>
        <Button href={`/article/${props.article.id}`} size="small">Learn More</Button>
      </CardActions>
    </Card>
  );
}

export default Article;
