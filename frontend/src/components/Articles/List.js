import React from "react";
import Article from "./Article";
import Api from "../../params/env";
import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import {Grid} from "@mui/material";
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
import PermanentStorage from "../../storage";
import {get, getCsrf} from "../../request";

const token = PermanentStorage.getItem('token') || false;
class List extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      articles: [],
      isLoading: false,
      meta: null,
      page: 1
    };
  }


  async getArticles(page = 1) {
    const path = `/articles/?page=${page}`
    try {
      this.setState({isLoading: true});
      getCsrf().then(r => {
        get(Api.urlV1(path), token).then(response => {
          this.setState({articles: response.data, isLoading: false, meta: response.meta})
        })
      })
    } catch (err) {
      this.setState({isLoading: false});
      console.error(err);
    }
  }

  componentDidMount() {
    if (!token) {
      window.location.href = '/login';
    }
    this.getArticles().then();
  }

  handleChangePage(e) {
    const page = parseInt(e.currentTarget.innerText);
    this.setState({
      ...this.state,
      page: page
    });
    this.getArticles(page).then();
  }

  render() {
    const isLoading = this.state.isLoading;

    if (isLoading) {
      return (
        <Box sx={{display: 'flex'}}>
          <CircularProgress/>
        </Box>
      )
    } else {
      return (
        <>
          <Grid container spacing={{xs: 2, md: 3}} columns={{xs: 4, sm: 8, md: 12}}>
            {!isLoading && this.state.articles.map((article, key) => (
              <Grid item xs={2} sm={4} md={4} key={key}>
                <Article article={article}/>
              </Grid>
            ))}
          </Grid>
          <br/>
          <Stack spacing={2}>
            <Pagination
              page={this.state.page}
              count={this.state.meta?.last_page}
              variant="outlined"
              shape="rounded"
              onChange={(e) => this.handleChangePage(e)}
            />
          </Stack>
        </>
      );
    }
  }
}

export default List;
