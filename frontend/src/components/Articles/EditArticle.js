import {useParams} from "react-router-dom";
import PermanentStorage from "../../storage";
import {useEffect, useState} from "react";
import {get, getCsrf} from "../../request";
import Api from "../../params/env";
import {LinearProgress} from "@mui/material";
import Form from "./Form";


export default function EditArticle() {
  const params = useParams();
  const token = PermanentStorage.getItem('token') || false;
  const [article, setArticle] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    console.log(params, token);
    if (params?.id && token) {
      setIsLoading(true);
      getCsrf().then(r => {
        get(Api.urlV1(`/articles/${params.id}`), token).then(response => {
          const article = response.data;
          if (article.can_edit) {
            setArticle(article);
          } else {
            window.location.href = '/articles';
          }
          setIsLoading(false);
        })
      })
    } else {
      window.location.href = '/login';
    }
  }, []);

  return (
    <>
      {isLoading && <LinearProgress />}
      {!isLoading && article && <Form article={article} isCreate={false}/>}
    </>
  );
}
