import React, {Component} from 'react';
import { DataGrid, GridActionsCellItem } from '@mui/x-data-grid';
import DeleteIcon from '@mui/icons-material/Delete';
import BorderColorIcon from '@mui/icons-material/BorderColor';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
import {API_URL, API_V1} from "../../params/env";

const columns = [
  { field: 'id', headerName: 'ID', width: 70 },
  { field: 'title', headerName: 'Article title', width: 350 },
  { field: 'body', headerName: 'Article body', width: 130 },
  {
    field: 'user',
    headerName: 'Author',
    width: 250,
    valueGetter: (params) => {
      return params.row.user.id;
    }
  },
  {
    field: 'actions',
    type: 'actions',
    getActions: (params) => [
      <GridActionsCellItem icon={<DeleteIcon />} label="Delete" />,
      <GridActionsCellItem icon={<BorderColorIcon/>} label="Edit" />,
    ]
  }
];

export default class ArticleTableComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      articles: null,
      isLoading: false,
      meta: null
    };
  }

  async getArticles() {
    if (!this.state.articles) {
      try {
        this.setState({ isLoading: true });
        const response = await fetch(API_URL + API_V1 + '/articles', {
          headers: {
            Accept: 'application/json'
          }
        });
        const articleList = await response.json();
        this.setState({articles: articleList.data, isLoading: false, meta: articleList.meta})
      } catch (err) {
        this.setState({ isLoading: false });
        console.error(err);
      }

    }
  }

  componentDidMount() {
    this.getArticles().then();
  }


  render() {
    const isLoading = this.state.isLoading;

    if (isLoading) {
      return (
        <Box sx={{ display: 'flex' }}>
          <CircularProgress />
        </Box>
      )
    } else {
      return (
        <>
          {this.state.articles &&
          <DataGrid
            rows={this.state.articles}
            columns={columns}
            initialState={{
              pagination: {
                paginationModel: { page: this.state.meta.current_page, pageSize: this.state.meta.per_page },
              },
            }}
            pageSizeOptions={[8, 16]}
            checkboxSelection
            autoHeight
          />
          }
        </>
      );
    }
  };
}
