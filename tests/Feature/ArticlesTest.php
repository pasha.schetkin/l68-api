<?php

namespace Tests\Feature;

use Tests\TestCase;

class ArticlesTest extends TestCase
{

    private string $url;

    public const DATA = [
        'title' => 'test',
        'body' => 'body test',
        'user_id' => null
    ];

    protected function setUp(): void
    {
        parent::setUp();
        $this->url = route('articles.store');
    }

    /**
     * @group articles
     */
    public function test_success_create_article(): void
    {
        $data = self::DATA;
        $data['user_id'] = $this->user->id;
        $this->assertDatabaseMissing('articles', $data);
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(201);
        $response->assertJsonStructure(
            [
                'data' => [
                    'title', 'body', 'id', 'user'
                ]
            ]
        );

        $this->assertDatabaseHas('articles', $data);
    }

    /**
     * @group articles
     */
    public function test_trying_create_article_without_title(): void
    {
        $data = self::DATA;
        $data['user_id'] = $this->user->id;
        $data['title'] = null;
        $this->assertDatabaseMissing('articles', $data);
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(422);
        $response->assertJsonStructure(
            [
                'message',
                'errors' => [
                    'title'
                ]
            ]
        );

        $this->assertDatabaseMissing('articles', $data);
    }

    /**
     * @group articles
     */
    public function test_trying_create_article_without_body(): void
    {
        $data = self::DATA;
        $data['user_id'] = $this->user->id;
        $data['body'] = null;
        $this->assertDatabaseMissing('articles', $data);
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(422);
        $response->assertJsonStructure(
            [
                'message',
                'errors' => [
                    'body'
                ]
            ]
        );

        $this->assertDatabaseMissing('articles', $data);
    }

    /**
     * @group articles
     */
    public function test_trying_create_article_without_user_id(): void
    {
        $data = self::DATA;
        $this->assertDatabaseMissing('articles', $data);
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(422);
        $response->assertJsonStructure(
            [
                'message',
                'errors' => [
                    'user_id'
                ]
            ]
        );

        $this->assertDatabaseMissing('articles', $data);
    }

    /**
     * @group articles
     */
    public function test_trying_create_article_without_all_data(): void
    {
        $data = [];
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(422);
        $response->assertJsonStructure(
            [
                'message',
                'errors' => [
                    'title',
                    'body',
                    'user_id'
                ]
            ]
        );
    }
}
